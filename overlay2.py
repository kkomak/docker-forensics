# -*- coding:utf-8 -*-

__author__  = "Kim, Taehoon(kimfrancesco@gmail.com)"

import sys
from subprocess import Popen, PIPE
import argparse
import json

UPPDERDIR = "UpperDir"
DOCKER_INSPECT_CMD ="docker inspect {}"
class OverlayFS:
    def __init__(self):
        pass

    def get_upperlayer_path(self, container_id):
        p = Popen(DOCKER_INSPECT_CMD.format(container_id), shell=True, stdout=PIPE, stderr=PIPE)
        data_dump, stderr_data = p.communicate()
        print(json.dumps(json.loads(data_dump), indent=4))
        data = (json.loads(data_dump))
        print(data[0]['GraphDriver']['Data']['UpperDir'])



def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--container_id', required=True, action='store')
    args = parser.parse_args()

    fs = OverlayFS()
    fs.get_upperlayer_path(args.container_id)

if __name__ == "__main__":
    main()
